package com.plenum.beastchat.services;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Looper;
import android.util.Log;
import android.util.Patterns;
import android.widget.EditText;
import android.widget.Toast;

import com.plenum.beastchat.activities.LoginActivity;
import com.plenum.beastchat.activities.RegisterActivity;

import org.json.JSONException;
import org.json.JSONObject;
import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by jcastro on 02/02/2017.
 */

public class LiveAccountServices extends AsyncTask<Void,Void,Integer>{
    private static LiveAccountServices mLiveAccountServices;

    private final String LOG_TAG = LiveAccountServices.class.getSimpleName();

    private final int USER_ERROR_EMPTY_PASSWORD = 1;
    private final int USER_ERROR_EMPTY_EMAIL = 2;
    private final int USER_ERROR_EMPTY_USERNAME = 3;
    private final int USER_ERROR_PASSWORD_SHORT = 4;
    private final int USER_ERROR_EMAIL_BAD_FORMAT = 5;

    private final int SERVER_SUCCESS = 6;
    private final int SERVER_FAILURE = 7;

    private EditText mUserNameEt;
    private EditText mUserEmailEt;
    private EditText mUserPasswordEt;
    private Socket mSocket;
    private Context mContext;
    private ProgressDialog mProgressDialog;

    public static LiveAccountServices getInstance(Context context, final EditText userNameEt, final EditText userEmailEt,
                                           final EditText userPasswordEt, final Socket socket){
        if(mLiveAccountServices == null)
            mLiveAccountServices = new LiveAccountServices(context, userNameEt, userEmailEt,
                    userPasswordEt, socket);
        return mLiveAccountServices;
    }

    private LiveAccountServices(Context context, final EditText userNameEt, final EditText userEmailEt,
                          final EditText userPasswordEt, final Socket socket){
        mContext = context;
        mUserNameEt = userNameEt;
        mUserEmailEt = userEmailEt;
        mUserPasswordEt = userPasswordEt;
        mSocket = socket;
        mProgressDialog = new ProgressDialog(mContext);
        mProgressDialog.setMessage("Registrando usuario");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setCancelable(false);

        registerResponse(mSocket);
    }

    private void registerResponse(Socket socket){
        socket.on("message", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                final JSONObject response = (JSONObject) args[0];
                final RegisterActivity activity = (RegisterActivity) mContext;
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mProgressDialog.dismiss();
                        try {
                            String message = (String) response.getJSONObject("message").get("text");
                            Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                            if(message.toLowerCase().equals("success"))
                                activity.finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });
    }

    private Integer sendRegistrationInfo(){

        String userName = mUserNameEt.getText().toString();
        String userEmail = mUserEmailEt.getText().toString();
        String userPassword = mUserPasswordEt.getText().toString();

        if(userName.isEmpty()){
            return USER_ERROR_EMPTY_USERNAME;
        }
        else if(userEmail.isEmpty()){
            return USER_ERROR_EMPTY_EMAIL;
        }
        else if(userPassword.isEmpty()){
            return USER_ERROR_EMPTY_PASSWORD;
        }
        else if(userPassword.length() < 6){
            return USER_ERROR_PASSWORD_SHORT;
        }
        else if(!isEmailValid(userEmail)){
            return USER_ERROR_EMAIL_BAD_FORMAT;
        }
        else {
            JSONObject sendData = new JSONObject();
            try{
                sendData.put("email", userEmail);
                sendData.put("userName", userName);
                sendData.put("password", userPassword);
                mSocket.emit("userData", sendData);
                return SERVER_SUCCESS;
            }catch (JSONException e){
                e.printStackTrace();
                Log.i(LOG_TAG, e.getMessage());
                return SERVER_FAILURE;
            }
        }
    }

    private boolean isEmailValid(CharSequence email){
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    protected void onPreExecute() {
        mProgressDialog.show();
    }

    @Override
    protected Integer doInBackground(Void... params) {
        return sendRegistrationInfo();
    }

    @Override
    protected void onPostExecute(Integer integer) {
        if(integer != SERVER_FAILURE && integer != SERVER_SUCCESS)
            mProgressDialog.dismiss();
        switch (integer){
            case USER_ERROR_EMPTY_USERNAME:
                mUserNameEt.setError("El nombre de usuario es obligatorio");
                break;
            case USER_ERROR_EMPTY_EMAIL:
                mUserEmailEt.setError("El email es obligatorio");
                break;
            case USER_ERROR_EMAIL_BAD_FORMAT:
                mUserEmailEt.setError("El email tiene un formato equivocado");
                break;
            case USER_ERROR_EMPTY_PASSWORD:
                mUserPasswordEt.setError("El password es obligatorio");
                break;
            case USER_ERROR_PASSWORD_SHORT:
                mUserPasswordEt.setError("El password debe tener al menos 6 caracteres");
                break;
            case SERVER_FAILURE:
                Toast.makeText(mContext, "Error en el servidor", Toast.LENGTH_LONG).show();
                break;
        }
        mLiveAccountServices = null;
    }
}
