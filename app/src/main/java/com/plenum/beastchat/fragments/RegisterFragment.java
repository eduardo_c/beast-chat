package com.plenum.beastchat.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.plenum.beastchat.R;
import com.plenum.beastchat.R2;
import com.plenum.beastchat.services.LiveAccountServices;
import com.plenum.beastchat.utils.Constants;

import java.net.URISyntaxException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by jcastro on 01/02/2017.
 */

public class RegisterFragment extends BaseFragment {

    private final String LOG_TAG = RegisterFragment.class.getSimpleName();

    @BindView(R2.id.fragment_register_userName)
    EditText mUserNameEt;

    @BindView(R2.id.fragment_register_userEmail)
    EditText mUserEmailEt;

    @BindView(R2.id.fragment_register_userPassword)
    EditText mUserPasswordEt;

    @BindView(R2.id.fragment_register_loginButton)
    Button mLoginButton;

    @BindView(R2.id.fragment_register_registerButton)
    Button mRegisterButton;

    private Unbinder mUnbinder;
    private Socket mSocket;
    private LiveAccountServices mLiveAccountServices;


    public static RegisterFragment newInstance(){
        return new RegisterFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            mSocket = IO.socket(Constants.IP_LOCAL_HOST);
        } catch (URISyntaxException e) {
            Log.e(LOG_TAG, e.getMessage());
            Toast.makeText(getActivity(), "Can't connect to the server", Toast.LENGTH_LONG).show();
        }

        mSocket.connect();

    }

    @OnClick(R2.id.fragment_register_registerButton)
    public void setmRegisterButton(){
        mLiveAccountServices = LiveAccountServices.getInstance(getActivity(), mUserNameEt,
                    mUserEmailEt, mUserPasswordEt, mSocket);
        mLiveAccountServices.execute();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_register, container, false);
        mUnbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();
    }
}
