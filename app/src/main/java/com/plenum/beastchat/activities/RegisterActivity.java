package com.plenum.beastchat.activities;

import android.support.v4.app.Fragment;

import com.plenum.beastchat.fragments.BaseFragment;
import com.plenum.beastchat.fragments.RegisterFragment;

/**
 * Created by jcastro on 01/02/2017.
 */

public class RegisterActivity extends BaseFragmentAcitivity {

    @Override
    Fragment createFragment() {
        return RegisterFragment.newInstance();
    }
}
