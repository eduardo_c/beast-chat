package com.plenum.beastchat.activities;


import android.support.v4.app.Fragment;

import com.plenum.beastchat.fragments.LoginFragment;

public class LoginActivity extends BaseFragmentAcitivity {

    private final String LOG_TAG = LoginActivity.class.getSimpleName();

    @Override
    Fragment createFragment() {
        return LoginFragment.newInstance();
    }
}
